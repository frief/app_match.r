# what is it?

**app_match.R** is an R-script which finds similar apps on f-droid by similarity of their descriptions.

Output is  
- a list of all f-droid apps with their respective 'closest' matches
- a markdown file app_match.md with the respective 'closest' matches. Depending on the viewer you are using a click on the app name will take you to the app page on f-droid
- diagrams for *each* of the apps (descriptions, antifeatures/permissions,  etc.)

# what's the status?

It is run on a weekly schedule on GitLab's **CI platform** and produces the output mentioned above.

A slightly different or older version of **app_match.R** is used for the "similar apps" view on [g-droid](https://gitlab.com/gdroid/gdroidclient/).

# why does it exist?

Commercial app stores host millions of apps. And they find a way to let users install the apps the users are interested in.
Although F-Droid hosts around a factor of thousand less apps I personally found it challenging to install the set of applications I cared about (this was 2018).

For commercial app stores there are several approaches to suggest applications (among them: highest rated, most downloaded, downloaded near your location, installed on home page, users who installed this also installed that, app is most similar to, amount of time spend on, number of user interactions, users like you use this app at this time of day, users like you when teased with x and y installed y, your status in your social network might increase by).  
Yes, it is creepy. But "allow us to improve our service" allows for this (and more).

TLDR. Anyway. These approaches exist.

Back to my starting point *install the set of applications I cared about*. I wanted to address one approach which is possible without leaking data: find an app similar to another app based on their descriptions.
Not more, not less.

# does it give reasonable results?

I was surprised how good it works. (Obviously I'm biased)

# can I see results without installing anything?

Yes. Download one of the zipped files at (https://gitlab.com/frief/app_match.r/pipelines) .

# can multiple repositories be used?

Yes.

# does it run on a Raspberry Pi?

Glad you asked, yes:)

You need some patience and a Raspbian installation on a [Raspberry Pi](https://www.raspberrypi.org/) (I checked with Raspberry Pi 4, 2 GB). Then this should work:

```
sudo apt-get update  
sudo apt-get install git unzip build-essential curl libxml2-dev libpoppler-cpp-dev r-base r-base-dev  
sudo apt-get install --no-install-recommends r-cran-stringi r-cran-getopt r-cran-tm r-cran-snowballc r-cran-pheatmap r-cran-jsonlite r-cran-data.table  
sudo apt-get clean  
#  
git clone https://gitlab.com/frief/app_match.r.git  
cd app_match.r  
mkdir --parent ./R/library  
./runit.sh --mtime added --plotselectadded 14 -m 0xff -d out  
```
Expect about two hours for the first run. Output for some recently added apps is in the directory `out`. Subsequent runs are faster.

# lessons learned?

I imagined *app_match.R* would be picked up like the best invention since the toaster and be quickly absorbed into the F-droid client.
Well, this did not happen.
Instead it was immediately picked up by Andreas Redmer (G-Droid, link above) in a time zone about 12h from here. Now this is cool! And this might just be what the internet is about:)

# next steps?

app_match.R could be used for a monthly summary of apps included in F-Droid, within the F-Droid client [#957](https://gitlab.com/fdroid/fdroidclient/-/issues/957), to inform authors of newly added apps about similar code, to find alternative apps which require less permissions, to navigate within F-Droid's web pages: https://f-droid.org/en/packages/ .

# communication

There's a dedicated thread in the forum
[app_match.R - find similar apps on f-droid by similarity of their descriptions](https://forum.f-droid.org/t/app-match-r-find-similar-apps-on-f-droid-by-similarity-of-their-descriptions/4844)

# examples

Examples of the plots that can be generated:  
<img src="sample-out/am_heatmap_de.k3b.android.androFotoFinder.1.png" width="360" height="360" />
focus on the terms that the matching is based on  
<img src="sample-out/am_heatmap_de.schildbach.oeffi.2.png" width="360" height="360" />
focus on description  
<img src="sample-out/am_heatmap_org.fdroid.fdroid.3.png" width="540" height="360" />
focus on Antifeatures and Android.Permissions
